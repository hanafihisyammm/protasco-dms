//
//  SubmitOrAddDefectViewController.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 17/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

protocol SubmitOrAddDefectViewControllerDelegate {
    func cleanFloorPlanDelegate()
}

class SubmitOrAddDefectViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var txvDescription: UITextView!
    @IBOutlet weak var cvImages: UICollectionView!
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            self.btnBack.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
            self.delegate?.cleanFloorPlanDelegate()
        }
    }
    
    var defectTakenImage = [UIImage]()
    var imageSize = CGSize()
    
    var groupCreatedData = [String: Any]()
    var createDate = [[String: Any]]()
    
    var unitNo = String()
    var projectTypeID = Int()
    var groupId = ""
    var groupDateTimeCreated = ""
    var delegate: SubmitOrAddDefectViewControllerDelegate?
    var floorPlanX = CGFloat()
    var floorPlanY = CGFloat()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(projectTypeID)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SubmitOrAddDefectViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SubmitOrAddDefectViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        hideKeyboardWhenTappedAround()
        
        
        txvDescription.delegate = self
        
        cvImages.dataSource = self
        cvImages.delegate = self
        cvImages.reloadData()
        
        print(floorPlanX)
        print(floorPlanY)
        
        floorPlanX = floorPlanX * 1.6667
        floorPlanY = floorPlanY * 1.6667
        
        print(floorPlanX)
        print(floorPlanY)
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if txvDescription.text == "Describe defect here..." {
            txvDescription.text = ""
            txvDescription.textColor = UIColor.black
            txvDescription.font = UIFont(name: "Avenir Next", size: 14)
        }
    }
    
    @IBAction func didTapSubmitButton(_ sender: Any) {
        var imageData: Data?
        var imageFilename: String?
        
        imageData = defectTakenImage[0].jpegData(compressionQuality: 1)
        imageFilename = "DefectImage.jpg"
        
        groupCreatedData = ["unit_no": unitNo,
                            "project_type_id": projectTypeID]
        
        
        if groupId == "" {
            Constant.networkController.postGroupCreate(groupCreateData: groupCreatedData) { (groupCreate) in
                self.groupId = groupCreate.group
                self.groupDateTimeCreated = groupCreate.date_time_created
                Constant.dipatchMainAsync.async {
                    if !self.createDate.isEmpty {
                        self.createDate.append(["unit_no": self.unitNo,
                                                "description": self.txvDescription.text!,
                                                "group": self.groupId,
                                                "date_time_created": self.groupDateTimeCreated,
                                                "project_type_id": self.projectTypeID,
                                                "status": "New",
                                                "floorplan_x": Double(self.floorPlanX),
                                                "floorplan_y": Double(self.floorPlanY)])
                        
                        let count = self.createDate.count
                        for i in stride(from: 0, to: count, by: 1) {
                            
                            self.createDate[i]["group"] = self.groupId
                            self.createDate[i]["date_time_created"] = self.groupDateTimeCreated
                            
                            Constant.networkController.postCreate(createData: self.createDate[i], imageKey: "image", imageData: imageData, imageFilename: imageFilename) { (create) in
                                print("what is this", create)
                                self.dismiss(animated: true)
                            }
                        }
                    } else {
                        print("groupId", self.groupId)
                        self.createDate.append(["unit_no": self.unitNo,
                                                "description": self.txvDescription.text!,
                                                "group": self.groupId,
                                                "date_time_created": self.groupDateTimeCreated,
                                                "project_type_id": self.projectTypeID,
                                                "status": "New",
                                                "floorplan_x": Double(self.floorPlanX),
                                                "floorplan_y": Double(self.floorPlanY)])
                        
                        let count = self.createDate.count
                        
                        for i in stride(from: 0, to: count, by: 1) {
                            
                            self.createDate[i]["group"] = self.groupId
                            self.createDate[i]["date_time_created"] = self.groupDateTimeCreated
                            
                            Constant.networkController.postCreate(createData: self.createDate[i], imageKey: "image", imageData: imageData, imageFilename: imageFilename) { (create) in
                                print(create)
                                self.dismiss(animated: true)
                            }
                        }
                        print(self.createDate)
                    }
                }
            }
        } else {
            
        }
        print(createDate)
    }
    @IBAction func didAddButton(_ sender: Any) {
        self.createDate.append(["unit_no": unitNo,
                                "description": txvDescription.text!,
                                "group": groupId,
                                "date_time_created": "",
                                "project_type_id": projectTypeID,
                                "status": "New",
                                "floorplan_x": self.floorPlanX,
                                "floorplan_y": self.floorPlanY])
        Constant.defaults.set(createDate, forKey: "UploadDefectData")
        self.dismiss(animated: true)
        print(createDate)
    }
}

extension SubmitOrAddDefectViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DisplayDefectTakenImageCVCell", for: indexPath) as? DisplayDefectTakenImageCVCell else { return UICollectionViewCell() }
        
        cell.ivDefect.image = defectTakenImage[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! DisplayDefectTakenImageCVCell
        self.imageTapped(image: cell.ivDefect.image!)
    }

    @objc private func imageTapped(image:UIImage){
        
        let newImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        newImageView.image = image
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        
        newImageView.backgroundColor = .black
                
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        
        UIView.transition(with: self.view, duration: 0.3, options: [.transitionCrossDissolve], animations: {
          self.view.addSubview(newImageView)
        }, completion: nil)
            
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        
    }

    @objc private func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
}
