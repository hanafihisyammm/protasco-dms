//
//  GroupDefectController.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 27/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

class GroupDefectController: UIViewController {

    @IBOutlet weak var tvGroupDefect: UITableView!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            self.backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var createDefectButton: UIButton!
    
    //Pass from MainController
    var projectID: Int!
    var unit: String!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tvGroupDefect.dataSource = self
        tvGroupDefect.delegate = self
        loadGrouDefectData()
        
    }
    
    private func loadGrouDefectData() {
        Constant.networkController.getGroupDefect(projectID: projectID, unit: unit) { (groupDefectList) in
            print(groupDefectList)
        }
    }
    
    @IBAction func didTapCreateDefectButton(_ sender: Any) {
        
    }
}

extension GroupDefectController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupDefectCell", for: indexPath) as? GroupDefectCell else { return UITableViewCell() }
        
        return cell
    }
    
    
}
