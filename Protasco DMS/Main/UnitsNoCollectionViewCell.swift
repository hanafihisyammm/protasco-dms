//
//  UnitsNoCollectionViewCell.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 23/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

class UnitsNoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblUnitNo: UILabel!
    
}
