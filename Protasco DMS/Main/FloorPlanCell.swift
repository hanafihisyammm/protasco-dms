//
//  FloorPlanCell.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 19/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

class FloorPlanCell: UICollectionViewCell {
    
    @IBOutlet weak var ivFloorPlan: UIImageView!
    
    @IBOutlet weak var constraintFloorPlanImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintFloorPlanImageViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let pinch = UIPinchGestureRecognizer()
        pinch.addTarget(self, action: #selector(pinchGesture))
        ivFloorPlan.addGestureRecognizer(pinch)
        print("here")
        
    }
    
    @objc func pinchGesture(sender: UIPinchGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            let currentScale = ivFloorPlan.frame.size.width / ivFloorPlan.bounds.size.width
            let newScale = currentScale*sender.scale
            let transform = CGAffineTransform(scaleX: newScale, y: newScale)
            ivFloorPlan.transform = transform
            sender.scale = 1
        }
    }
}
