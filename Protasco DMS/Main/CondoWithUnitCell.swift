//
//  CondoWithUnitCell.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 03/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

protocol CondoWithUnitCellDelegate {
    func didTapUnitCellDelegate(projectName: String, unit: String, floorPlan: [String], projectID: Int)
    func displayMySubmissionDelegate()
}

class CondoWithUnitCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var cvUnit: UICollectionView!
    @IBOutlet weak var cvUnitHeight: NSLayoutConstraint!
    
    var unitsNo = [String]()
    
    var projectID = Int()
    var unit = String()
    var projectName = String()
    var projectFloorPlan = String()
    
    var delegate: CondoWithUnitCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cvUnit.dataSource = self
        cvUnit.delegate = self
        
        cvUnit.semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
        cvUnit.reloadData()
        
    }
    @IBAction func didTapMySubmissionButton(_ sender: Any) {
        delegate?.displayMySubmissionDelegate()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return unitsNo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UnitsNoCollectionViewCell", for: indexPath) as? UnitsNoCollectionViewCell else { return UICollectionViewCell() }
        let unitNo = unitsNo[indexPath.row]
        cell.lblUnitNo.text = unitNo
//        let projectId = projectIDs[indexPath.row]
//        cell.tag = projectId
        
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 5
        cell.layer.shadowOpacity = 0.7
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didTapUnitCellDelegate(projectName: projectName, unit: unitsNo[indexPath.row], floorPlan: [projectFloorPlan], projectID: projectID)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
