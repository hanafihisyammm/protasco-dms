//
//  GroupDefectCell.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 05/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

protocol GroupDefectCellDelegate {
    func showCamera(positionX: CGFloat, positionY: CGFloat)
}

class PinPointDefectCell: UICollectionViewCell, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var ivFloorPlan: UIImageView!
    
    let sizeImg = CGSize(width: 300, height: 300)
    var delegate: GroupDefectCellDelegate?
    var marker = UIImageView()
    var imagePicker = UIImagePickerController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let pinch = UIPinchGestureRecognizer()
        pinch.addTarget(self, action: #selector(pinchGesture))
        ivFloorPlan.addGestureRecognizer(pinch)
        
        let pan = UIPanGestureRecognizer()
        pan.addTarget(self, action: #selector(panGesture))
        ivFloorPlan.addGestureRecognizer(pan)
        
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(tapGesture))
        ivFloorPlan.addGestureRecognizer(tap)
        
        
    }
    
    @objc func pinchGesture(sender: UIPinchGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            let currentScale = sender.view?.layer.value(forKeyPath: "transform.scale.x") as! CGFloat
            
            let minScale: CGFloat = 1.0
            let maxScale: CGFloat = 2.0
            let zoomSpeed: CGFloat = 2
            
            var deltaScale = sender.scale
            
            deltaScale = ((deltaScale - 1) * zoomSpeed) + 1
            deltaScale = min(deltaScale, maxScale/currentScale)
            deltaScale = max(deltaScale, minScale/currentScale)
            
            let zoomTransform: CGAffineTransform = sender.view!.transform.scaledBy(x: deltaScale, y: deltaScale)
            sender.view!.transform = zoomTransform
            sender.scale = 1
            
            if ivFloorPlan.frame.size == sizeImg {
                UIView.animate(withDuration: 0.5) {
                    self.ivFloorPlan.frame.origin.x = self.contentView.frame.width / 2 - self.ivFloorPlan.frame.width / 2
                    self.ivFloorPlan.frame.origin.y = 0
                }
            }
        } else if sender.state == .ended {
            if ivFloorPlan.frame.size == sizeImg {
                UIView.animate(withDuration: 0.5) {
                    self.ivFloorPlan.transform = CGAffineTransform.identity
                }
            }
        }
    }
    
    @objc func panGesture(sender: UIPanGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            let point = sender.location(in: self.superview)
            if ivFloorPlan.frame.size != sizeImg {
                if let superview = self.ivFloorPlan {
                    let restrictByPoint : CGFloat = 50.0
                    let superBounds = CGRect(x: superview.bounds.origin.x + restrictByPoint, y: superview.bounds.origin.y + restrictByPoint, width: superview.bounds.size.width - 2*restrictByPoint, height: superview.bounds.size.height - 2*restrictByPoint)
                    
                    if (superBounds.contains(point)) {
                        let translation = sender.translation(in: ivFloorPlan)
                        sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y + translation.y)
                        sender.setTranslation(CGPoint.zero, in: self.ivFloorPlan)
                    }
                }
            }
        }
    }
    
    @objc func tapGesture(sender: UITapGestureRecognizer) {
        
        let position = sender.location(in: ivFloorPlan)
        
        marker = UIImageView(frame: CGRect(x: 150, y: 150, width: 35, height: 35))
        marker.image = UIImage(named: "Marker")
        
        marker.backgroundColor = .black
        print(ivFloorPlan.frame.size)
        print(marker.frame.origin)
        //ivFloorPlan.addSubview(marker)
        
        
        let view1 = UIView(frame: CGRect(x: 0, y: 150, width: 300, height: 1))
        view1.backgroundColor = .black
        
        let view2 = UIView(frame: CGRect(x: 150, y: 0, width: 1, height: 300))
        view2.backgroundColor = .black
        ivFloorPlan.addSubview(view1)
        ivFloorPlan.addSubview(view2)
        
        let positionX = position.x
        let positionY = position.y
        
        delegate?.showCamera(positionX: 150, positionY: 150)
        print(position)
    }
}
