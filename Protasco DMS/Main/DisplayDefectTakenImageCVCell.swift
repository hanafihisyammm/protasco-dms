//
//  DisplayDefectTakenImageCVCell.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 25/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

class DisplayDefectTakenImageCVCell: UICollectionViewCell {
    
    @IBOutlet weak var ivDefect: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(tapGesture))
        ivDefect.addGestureRecognizer(tap)
        
    }
    
    @objc private func tapGesture(sender: UITapGestureRecognizer) {
        
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        
    }
    
}
