//
//  GroupDefectsController.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 05/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit
import SDWebImage
import NotificationCenter

class PinPointDefectController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    // MARK: - Outlet
    @IBOutlet weak var lblHouseTitle: UILabel!
    @IBOutlet weak var cvGroupDefect: UICollectionView!
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            self.btnBack.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
        }
    }
    @IBOutlet weak var vSwipeBottomBar: UIView!
    @IBOutlet weak var constraintSwipeBottomBar: NSLayoutConstraint!
    @IBOutlet weak var overlayView: UIView!
    
    // MARK: - Variable
    //Pass from mainController
    var unit: String!
    var projectID: Int!
    var projectName: String!
    var imgFloorPlan: String!
    
    var displayFloorPlan = String()
    var groupDefects = [GroupDefect]()
    
    var floorPlanX: CGFloat = 0.0
    var floorPlanY: CGFloat = 0.0
    
    
    var animator: UIViewPropertyAnimator?
    
    let imagePicker = UIImagePickerController()
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        //loadGroupDefect()
        lblHouseTitle.text = projectName + " " + unit
        
        cvGroupDefect.dataSource = self
        cvGroupDefect.delegate = self
        displayFloorPlan = Constant.imgFloorPlan + imgFloorPlan
        
        cvGroupDefect.reloadData()
        
        let swipe = UISwipeGestureRecognizer()
        swipe.delegate = self
        swipe.addTarget(self, action: #selector(swipeGestureSwipeView))
        vSwipeBottomBar.addGestureRecognizer(swipe)
        
        createBottomView()

        let name = NSNotification.Name(rawValue: "BottomViewMoved")
        NotificationCenter.default.addObserver(forName: name, object: nil, queue: nil, using: receiveNotification(_:))
        
    }
    
    @objc private func swipeGestureSwipeView(sender: UISwipeGestureRecognizer) {
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            self.cvGroupDefect.reloadData()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            guard let image = info[.originalImage] as? UIImage else {
                print("No image found")
                return
            }
            
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SubmitOrAddDefectViewController") as! SubmitOrAddDefectViewController
            controller.delegate = self
            
            let resizedImage = self.resizeImage(image: image, targetSize: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
            controller.defectTakenImage.append(resizedImage)
            controller.imageSize = resizedImage.size
            controller.floorPlanX = self.floorPlanX
            controller.floorPlanY = self.floorPlanY
            controller.unitNo = self.unit
            controller.projectTypeID = self.projectID
            
            
            self.present(controller, animated: true)
        }
    }
    
    func createBottomView() {
        guard let sub = storyboard!.instantiateViewController(withIdentifier: "BottomSheetViewController") as? BottomSheetViewController else { return }
        self.addChild(sub)
        self.view.addSubview(sub.view)
        sub.didMove(toParent: self)
        sub.view.frame = CGRect(x: 0, y: view.frame.maxY - 100, width: view.frame.width, height: view.frame.height)
    }

    func subViewGotPanned(_ percentage: Int) {
        guard let propAnimator = animator else {
            animator = UIViewPropertyAnimator(duration: 3, curve: .linear, animations: {
                self.overlayView.alpha = 0.8
                self.overlayView.isHidden = false
                //self.someView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8).concatenating(CGAffineTransform(translationX: 0, y: -20))
            })
            animator?.startAnimation()
            animator?.pauseAnimation()
            return
        }
        propAnimator.fractionComplete = CGFloat(percentage) / 100
    }

    func receiveNotification(_ notification: Notification) {
        guard let percentage = notification.userInfo?["percentage"] as? Int else { return }
        subViewGotPanned(percentage)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    private func loadGroupDefect() {
        Constant.networkController.getGroupDefect(projectID: projectID, unit: unit) { (result) in
            self.groupDefects = result
            Constant.dipatchMainAsync.async {
                
            }
        }
    }
}

extension PinPointDefectController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PinPointDefectCell", for: indexPath) as? PinPointDefectCell else { return UICollectionViewCell() }
        cell.delegate = self
        cell.marker.removeFromSuperview()
        cell.ivFloorPlan.sd_setImage(with: URL(string: displayFloorPlan))
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 300)
    }
}

extension PinPointDefectController: GroupDefectCellDelegate {
    func showCamera(positionX: CGFloat, positionY: CGFloat) {
        Utilities.showImagePicker(view: self, imagePicker: imagePicker, collectionView: cvGroupDefect)
        floorPlanX = positionX
        floorPlanY = positionY
    }
}

extension PinPointDefectController: SubmitOrAddDefectViewControllerDelegate {
    func cleanFloorPlanDelegate() {
        cvGroupDefect.reloadData()
    }
}
