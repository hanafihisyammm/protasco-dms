//
//  GroupDefectCell.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 27/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

class GroupDefectCell: UITableViewCell {
    
    @IBOutlet weak var lblGroup: UILabel!
    @IBOutlet weak var lblTotalDefects: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var vProgressBar1: UIView!
    @IBOutlet weak var vProgressBar2: UIView!
    @IBOutlet weak var vProgressBar3: UIView!
    @IBOutlet weak var vProgressBar4: UIView!
    @IBOutlet weak var vProgressBar5: UIView!
    @IBOutlet weak var vProgressBar6: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    

}
