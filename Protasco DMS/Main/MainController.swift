//
//  MainController.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 03/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

class MainController: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var vTop: UIView!
    @IBOutlet weak var unitTableView: UITableView!
    
    //MARK: - Localiazable
    let YOUR_SUBMISSION = NSLocalizedString("your_submission", comment: "")
    
    //MARK: - Variable
    var myUnitData = [Projects]()
    
    //MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        unitTableView.dataSource = self
        unitTableView.delegate = self

        setupViewForIphoneXNotch()
        UISetup()
        loadMyUnitData()
        
        unitTableView.sectionHeaderHeight = UITableView.automaticDimension
        unitTableView.estimatedSectionHeaderHeight = 100
        
        unitTableView.rowHeight = UITableView.automaticDimension
        unitTableView.estimatedRowHeight = 100
    }
    
    
    //MARK: - Private function
    private func UISetup() {
        vTop.backgroundColor = UIColor.Red.MainRed
    }
    
    private func loadMyUnitData() {
        Constant.networkController.getMyUnitData { (result) in
            Constant.dipatchMainAsync.async {
                
                self.unitTableView.dataSource = self
                self.myUnitData = result.projects
                
                self.unitTableView.reloadData()
            }
        }
    }
    
}

//MARK: - Extention TableView
extension MainController: UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return myUnitData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CondoWithUnitCell", for: indexPath) as? CondoWithUnitCell else {
            return UITableViewCell()
        }
        
        cell.delegate = self
        cell.unitsNo = myUnitData[indexPath.section].units
        cell.projectName = myUnitData[indexPath.section].project_name_type
        cell.projectFloorPlan = myUnitData[indexPath.section].floor_plan ?? ""
        cell.projectID = myUnitData[indexPath.section].project_type_id
        
        cell.frame = tableView.bounds
        cell.layoutIfNeeded()
        cell.cvUnit.reloadData()
        cell.cvUnitHeight.constant = cell.cvUnit.collectionViewLayout.collectionViewContentSize.height
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // code the view for header
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as? CondoNameTableViewCell else {
            return UIView()
        }
        
        cell.lblHouseTitle.text = myUnitData[section].project_name_type
        
        if section == 0  {
            cell.vSeparator.backgroundColor = .white
            cell.constraintLeadingSeparatorView.constant = 0
            cell.constraintTraillingSeparatorView.constant = 0
        }
        
        return cell.contentView
    }
}

extension MainController: CondoWithUnitCellDelegate {
    func displayMySubmissionDelegate() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BottomSheetViewController") as! BottomSheetViewController

        presentDetail(controller)
        
    }
    
    
    func didTapUnitCellDelegate(projectName: String, unit: String, floorPlan: [String], projectID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "GroupDefectController") as! GroupDefectController
//        controller. = projectName
        controller.unit = unit
//        controller.imgFloorPlan = floorPlan[0]
        controller.projectID = projectID
        
        present(controller, animated: true)
    }
}
