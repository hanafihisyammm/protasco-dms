//
//  Network Controller.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 04/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation
import UIKit

class NetworkController {
    
    let urlComponents = URLComponents(string: "http://10.0.110.133:8001")!
    //let urlComponents = URLComponents(string: "http://127.20.10.4:8001")!
    //let urlComponents = URLComponents(string: "https://defect-management-system.herokuapp.com")!
    
    let defaultSession = URLSession(configuration: .default)
    

    func login(loginData: [String: Any], withCompletion completion: @escaping (String) -> Void) {
        var loginURLComponents = urlComponents
        loginURLComponents.path = "/api/login/"
        let loginURL = loginURLComponents.url!
        
        var request = URLRequest(url: loginURL)
        request.httpMethod = "POST"
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: loginData, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        Constant.defaults.set(loginURL, forKey: "request")
        
        let dataTask = defaultSession.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let user = try decoder.decode((User.self), from: data)
                
                if user.token != "" {
                    KeychainService.savePassword(service: "myService", account: user.name, data: user.token)
                }
                
                if user.name != "" {
                    Constant.defaults.set(user.name, forKey: "Username")
                }
                
                if user.status == 1 {
                    Constant.defaults.set(user.status, forKey: "LoginStatus")
                }
                print(user)
                completion("login successfully")
            }
            catch let error {
                print(error)
            }
        })
        
        dataTask.resume()
    }
    
    func getMyUnitData(withCompletion completion:  @escaping (MyUnitData) -> Void) {
        var myUnitDataURLComponents = urlComponents
        myUnitDataURLComponents.path = "/api/myunitdata/"
        let myUnitDataURL = myUnitDataURLComponents.url!
        
        
        var request = URLRequest(url: myUnitDataURL)
        request.httpMethod = "GET"
        
        let token = KeychainService.loadPassword(service: "myService", account: Constant.defaults.string(forKey: "Username")!)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Token " + token!, forHTTPHeaderField: "Authorization")
     
        let dataTask = defaultSession.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let myUnitData = try decoder.decode((MyUnitData.self), from: data)
                completion(myUnitData)
            }
            catch let error {
                print(error)
            }
        })
        
        dataTask.resume()
    }
    
    func getGroupDefect(projectID: Int, unit: String, withCompletion completion:  @escaping ([GroupDefect]) -> Void) {
        var groupDefectURLComponents = urlComponents
        groupDefectURLComponents.path = "/api/groupdefects/"
        
        let groupDefectQuery = URLQueryItem(name: "project_id", value: "\(projectID)")
        let groupDefectQuery1 = URLQueryItem(name: "unit_no", value: unit)
        groupDefectURLComponents.queryItems = [groupDefectQuery, groupDefectQuery1]
        
        let groupDefectURL = groupDefectURLComponents.url!
        
        var request = URLRequest(url: groupDefectURL)
        request.httpMethod = "GET"
        
        let token = KeychainService.loadPassword(service: "myService", account: Constant.defaults.string(forKey: "Username")!)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Token " + token!, forHTTPHeaderField: "Authorization")
        
        let dataTask = defaultSession.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let groupDefect = try decoder.decode(Array<GroupDefect>.self, from: data)
                completion(groupDefect)
            }
            catch let error {
                print(error)
            }
        })
        
        dataTask.resume()
    }
    
    func getDefectInGroup(group: String, withCompletion completion:  @escaping ([DefectInGroup]) -> Void) {
        var defectInGroupURLComponents = urlComponents
        defectInGroupURLComponents.path = "/api/defectsingroup/\(group)/"
        
        let defectInGroupURL = defectInGroupURLComponents.url!
        
        var request = URLRequest(url: defectInGroupURL)
        request.httpMethod = "GET"
        
        let token = KeychainService.loadPassword(service: "myService", account: Constant.defaults.string(forKey: "Username")!)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Token " + token!, forHTTPHeaderField: "Authorization")
        
        let dataTask = defaultSession.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let defectInGroup = try decoder.decode(Array<DefectInGroup>.self, from: data)
                completion(defectInGroup)
            }
            catch let error {
                print(error)
            }
        })
        
        dataTask.resume()
    }
    
    func postGroupCreate(groupCreateData: [String: Any], withCompletion completion: @escaping (GroupCreate) -> Void) {
        var postGroupCreateURLComponents = urlComponents
        postGroupCreateURLComponents.path = "/api/group/create/"
        let postGroupCreateURL = postGroupCreateURLComponents.url!
        
        var request = URLRequest(url: postGroupCreateURL)
        request.httpMethod = "POST"
        
        let token = KeychainService.loadPassword(service: "myService", account: Constant.defaults.string(forKey: "Username")!)
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: groupCreateData, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Token " + token!, forHTTPHeaderField: "Authorization")
        
        let dataTask = defaultSession.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let groupCreate = try decoder.decode((GroupCreate.self), from: data)
                
                completion(groupCreate)
            }
            catch let error {
                print(error)
            }
        })
        
        dataTask.resume()
    }
    
    
    func postCreate(createData: [String: Any], imageKey: String, imageData: Data?, imageFilename: String?, withCompletion completion: @escaping (Create) -> Void) {
        var postCreateURLComponents = urlComponents
        postCreateURLComponents.path = "/api/create/"
        let postCreateURL = postCreateURLComponents.url!
        
        var request = URLRequest(url: postCreateURL)
        request.httpMethod = "POST"
        
        let token = KeychainService.loadPassword(service: "myService", account: Constant.defaults.string(forKey: "Username")!)
        
        let boundary = Utilities.generateBoundary()
        
        request.addValue("Token " + token!, forHTTPHeaderField: "Authorization")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: createData, options: [])
               request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = Utilities.createBodyWithParameters(parameters: createData, imageKey: imageKey, imageData: imageData, imageFilename: imageFilename, boundary: boundary)
        
        let dataTask = defaultSession.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let groupCreate = try decoder.decode((Create.self), from: data)
                completion(groupCreate)
            }
            catch let error {
                print(error)
            }
        })
        
        dataTask.resume()
    }
}


