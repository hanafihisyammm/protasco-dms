//
//  Utilities.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 17/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation
import UIKit

public class Utilities {
    class func showImagePicker(view: UIViewController, imagePicker: UIImagePickerController, collectionView: UICollectionView) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let showCameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            showCamera(view: view, imagePicker: imagePicker)
        })
        let showPhotoLibraryAction = UIAlertAction(title: "Photo Library", style: .default, handler: { (action) -> Void in
            showPhotoLibrary(view: view, imagePicker: imagePicker)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (cancel) in
            collectionView.reloadData()
        }
        
        alertController.addAction(showCameraAction)
        alertController.addAction(showPhotoLibraryAction)
        alertController.addAction(cancelAction)
        view.present(alertController, animated: true)
    }
    
    class func showCamera(view: UIViewController, imagePicker: UIImagePickerController) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            view.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    class func showPhotoLibrary(view: UIViewController, imagePicker: UIImagePickerController) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            view.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    class func generateBoundary() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    class func createBodyWithParameters(parameters: [String: Any], imageKey: String, imageData: Data?, imageFilename: String?, boundary: String) -> Data {
        let mimeType = "image/jpeg"
        let lineBreak = "\r\n"
        var body = Data()
        
        for (key, value) in parameters {
            body.append("--\(boundary + lineBreak)")
            body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
            body.append("\(value)\(lineBreak)")
        }
        
        if let imageData = imageData {
            body.append("--\(boundary + lineBreak)")
            body.append("Content-Disposition: form-data; name=\"\(imageKey)\"; filename=\"\(imageFilename!)\"\(lineBreak)")
            body.append("Content-Type: \(mimeType + lineBreak + lineBreak)")
            body.append(imageData)
            body.append(lineBreak)
        }
        else {
            body.append("--\(boundary + lineBreak)")
            body.append("Content-Disposition: form-data; name=\"\(imageKey)\"\(lineBreak + lineBreak)")
            body.append("\(lineBreak)")
        }
        
        body.append("--\(boundary)--\(lineBreak)")
        
        return body
    }
}

