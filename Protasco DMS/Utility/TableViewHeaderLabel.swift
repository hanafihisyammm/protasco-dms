//
//  TableViewHeaderLabel.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 06/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation

import UIKit

class TableViewHeaderLabel: UILabel {
    
    var topInset:       CGFloat = 0
    var rightInset:     CGFloat = 12
    var bottomInset:    CGFloat = 0
    var leftInset:      CGFloat = 12
    
    override func drawText(in rect: CGRect) {
        
        let insets: UIEdgeInsets = UIEdgeInsets(top: self.topInset, left: self.leftInset, bottom: self.bottomInset, right: self.rightInset)
        self.setNeedsLayout()
        
        self.textColor = UIColor.white
        self.backgroundColor = UIColor.black
        
        self.lineBreakMode = .byWordWrapping
        self.numberOfLines = 0
        

        return super.drawText(in: rect)
    }
    
}
