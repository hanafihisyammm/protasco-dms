//
//  Font.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 03/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

struct Fonts {
    static let Regular = UIFont(name: "Font-Regular", size: 14)
}
