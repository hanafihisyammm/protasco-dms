//
//  Constant.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 03/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation

class Constant {
    static let mainColor = "FF473A"
    
    static let networkController = NetworkController()
    
    static let dipatchMainAsync = DispatchQueue.main
    static let defaults = UserDefaults.standard
    
    static let imgFloorPlan = "http://10.0.110.133:8001"
}
