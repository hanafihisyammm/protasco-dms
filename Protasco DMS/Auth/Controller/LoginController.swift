//
//  LoginController.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 03/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

class LoginController: UIViewController {
    
    // MARK: - Outlet
    //Textfield
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    //Button
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            self.btnBack.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var btnLogin: RippleButton!
    
    
    // MARK: - Localizable
    private let EMAIL = NSLocalizedString("email", comment: "")
    private let PASSWORD = NSLocalizedString("password", comment: "")
    private let LOGIN = NSLocalizedString("login", comment: "")
    
    //MARK: - Variable
    private var loginFlag = ""

    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UISetup()
        hideKeyboardWhenTappedAround()

    }
    
    //Mark: - Action
    @IBAction func didTapLoginButton(_ sender: RippleButton) {
    
        loginCredentioal()
        
        
        
        
    
    }
    
    // MARK: - Private function
    private func UISetup() {
        
        //Email & Textfield
        tfEmail.placeholder = EMAIL
        tfPassword.placeholder = PASSWORD
        
        //Login Button
        btnLogin.setTitle(LOGIN, for: .normal)
        btnLogin.backgroundColor = UIColor.Red.MainRed
        btnLogin.setTitleColor(UIColor.White.TextWhite, for: .normal)
        
    }
    
    private func loginCredentioal() {
        
        if tfEmail != nil || tfPassword != nil{
            let loginData = ["username": Int(tfEmail.text!)!,
                             "password": tfPassword.text!] as [String : Any]
            
            Constant.networkController.login(loginData: loginData) { (result) in
                self.loginFlag = result
                Constant.dipatchMainAsync.async {
                    if self.loginFlag == "login successfully" {
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let controller = storyboard.instantiateViewController(withIdentifier: "MainController")
                        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = controller
                        appDelegate.window!.makeKeyAndVisible()
                    }
                }
            }
        }
    }

}
