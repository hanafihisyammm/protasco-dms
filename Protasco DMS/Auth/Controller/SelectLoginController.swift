//
//  SelectLoginController.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 03/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import UIKit

class SelectLoginController: UIViewController {

    // MARK: - Outlet
    //Buttons
    @IBOutlet weak var btnOwner: RippleButton!
    @IBOutlet weak var btnContractor: RippleButton!
    
    // MARK: - Localizable
    private let OWNER = NSLocalizedString("owner", comment: "")
    private let CONTRACTOR = NSLocalizedString("contractor", comment: "")
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UISetup()
        

    }
    
    // MARK: - Action
    @IBAction func didTapButton(_ sender: RippleButton) {
        switch sender {
        case btnOwner:
            let storyboard = UIStoryboard(name: "Auth", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "LoginController")
            present(controller, animated: true)
        default:
            break
        }
        
    }
    
    // MARK: - Private function
    private func UISetup() {
        //Owner button
        btnOwner.setTitle(OWNER, for: .normal)
        btnOwner.backgroundColor = UIColor.Red.MainRed
        btnOwner.setTitleColor(UIColor.White.TextWhite, for: .normal)
        
        //Contractor button
        btnContractor.setTitle(CONTRACTOR, for: .normal)
        btnContractor.backgroundColor = UIColor.Red.MainRed
        btnContractor.setTitleColor(UIColor.White.TextWhite, for: .normal)
    }
    
    


}
