//
//  GroupDefect.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 05/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation

struct GroupDefect: Codable {
    let id: Int
    let project: String
    let group: String
    let unit_no: String
    let project_type_id: String
    let defects_id: String
    let owner: String
    let number_of_defects: Int
    let date_time_submitted: String
    let status: String
}
