//
//  MyUnitData.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 04/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation

struct MyUnitData: Codable {
    let projects: [Projects]
}

struct Projects: Codable {
    let units: [String]
    let project_name_type: String
    let project_type_id: Int
    let floor_plan: String?
}
