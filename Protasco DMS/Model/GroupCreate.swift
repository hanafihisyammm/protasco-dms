//
//  GroupCreate.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 17/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation

struct GroupCreate: Codable {
    let group: String
    let unit_no: String
    let project_type_id: String
    let date_time_created: String
}
