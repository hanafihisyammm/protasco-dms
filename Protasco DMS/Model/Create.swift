//
//  Create.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 17/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation

struct Create: Codable {
    let group: String
    let unit: String
    let description: String
}
