//
//  GroupInDefect.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 06/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation

struct DefectInGroup: Codable {
    let pk: Int
    let unit: String
    let status: String
    let date_time_created: String
}
