//
//  User.swift
//  Protasco DMS
//
//  Created by Hanafi Hisyam on 04/09/2019.
//  Copyright © 2019 Hanafi Hisyam. All rights reserved.
//

import Foundation

struct User: Codable {
    
    let token: String
    let user_id: Int
    let name: String
    let status: Int
    let non_field_errors: [String]?
}
